import 'dart:ui';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:qtec_product/Services/base_client.dart';
import 'package:qtec_product/product_list_model.dart';
import 'package:flutter_html/flutter_html.dart';
import 'details_page.dart';

class LandingPageView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Qtec',
      showPerformanceOverlay: false,

      // home: LoginPage (title: 'ovivashiBatayon'),
      home: LandingPageViewExtend(),
    );
  }
}

class LandingPageViewExtend extends StatefulWidget {
  @override
  _LandingPageStateConfig createState() => _LandingPageStateConfig();
}

class _LandingPageStateConfig extends State<LandingPageViewExtend> {
  List<Result> product_list = List<Result>.empty(growable: true);

  late Future<void> product_list_future;

  TextEditingController product = TextEditingController();
  bool search = false;
  final _formKey = GlobalKey<FormState>();
  int _count = 0;

  @override
  initState() {
    super.initState();
    product.text = "";

    product_list_future = CallFormsFromAPI('');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: SingleChildScrollView(
          reverse: false,
          child: Container(
              margin: EdgeInsets.all(15),
              child: Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    _searchServices(),
                    _listBuilderProduct(product_list_future),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              )),
        ),
      ),
    );
  }

  Future<List<Result>> CallFormsFromAPI(String product) async {
    String error = "";

    String url = '';

    if (search == false) {
      url =
          'https://panel.supplyline.network/api/product/search-suggestions/?limit=10&offset=10';
    } else {
      url =
          'https://panel.supplyline.network/api/product/search-suggestions/?limit=10&offset=10&search=$product';
    }
    var response = await BaseClient().GetMethod(url);

    try {
      if (response == null) {
      } else {
        try {
          product_list = [];
          Map<String, dynamic> map = json.decode(response);
          var a;
          Map<String, dynamic> data = map["data"];
          Map<String, dynamic> products = data["products"];
          List<dynamic> results = products["results"];
          var ab;

          for (int i = 0; i < results.length; i++) {
            Result fact = Result.fromJson(results[i]);

            product_list.add(fact);
          }
        } catch (Exception) {
          print(Exception);
        }
      }
    } finally {
      print('');
    }

    return product_list;
  }

  Widget _listBuilderProduct(Future<void> product_list_model_fut) {
    return FutureBuilder<void>(
      future: product_list_model_fut,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        debugPrint('Builder');
        switch (snapshot.connectionState) {
          case ConnectionState.done:
            if (snapshot.hasError)
              return Center(child: new Text('Loading...'));
            else {
              if (snapshot.hasData) {
                if (product_list.isNotEmpty) {
                  return Container(
                    padding: EdgeInsets.all(5),
                    height: MediaQuery.of(context).size.height - 100,
                    child: Column(
                      children: [
                        Expanded(
                          child: GridView.builder(
                            shrinkWrap: true,
                            primary: false,
                            itemCount: product_list.length,
                            itemBuilder: (BuildContext context, int index) {
                              String productName =
                                  snapshot.data[index].productName;

                              String image = snapshot.data[index].image;
                              String description = snapshot.data[index].description;

                              String sellingPrice = snapshot
                                  .data[index].charge.sellingPrice
                                  .toString();
                              String currentPrice = snapshot
                                  .data[index].charge.currentCharge
                                  .toString();
                              String discountCharge = snapshot
                                  .data[index].charge.discountCharge
                                  .toString();

                              String brandName = snapshot.data[index].brand.name;

                              List<dynamic> _picList = [];

                              _picList = snapshot.data[index].images.map((image) => image.image.toString()).toList();



                              String gg = snapshot.data[index].images[0].image.toString();

                              if (discountCharge == "null") {
                                discountCharge = "0.0";
                              } else {
                                discountCharge = discountCharge;
                              }
                              if (sellingPrice == "null") {
                                sellingPrice = "0.0";
                              } else {
                                sellingPrice = sellingPrice;
                              }
                              String profit =
                                  snapshot.data[index].charge.profit.toString();
                              if (profit == "null") {
                                profit = "0.0";
                              } else {
                                profit = profit;
                              }
                              String slug = snapshot.data[index].slug;

                              var ab;

                              return Container(
                                margin: EdgeInsets.only(bottom: 10),
                                child: InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                DetailPageViewExtend(
                                                  productName: productName,
                                                    sellingPrice: sellingPrice,
                                                    currentPrice: currentPrice,
                                                    profit: profit,
                                                  description: description,
                                                  imageList: _picList,
                                                  brandName: brandName,
                                                )));
                                  },
                                  child: Card(
                                    elevation: 5,
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Align(
                                            alignment: Alignment.topCenter,
                                            child: Container(
                                              width: 87,
                                              height: 117,
                                              child: FittedBox(
                                                  fit: BoxFit.fill,
                                                  child: Image.network(image)),
                                            ),
                                          ),
                                          Expanded(
                                              child: Text(
                                            productName,
                                            style: new TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold),
                                          )),
                                          Expanded(
                                            child: RichText(
                                              textAlign: TextAlign.start,
                                              text: TextSpan(
                                                children: <TextSpan>[
                                                  TextSpan(
                                                    text: 'ক্রয়',
                                                    style: new TextStyle(
                                                        fontSize: 10,
                                                        color:
                                                            Color(0xFF646464)),
                                                  ),
                                                  TextSpan(
                                                    text: ' ৳$currentPrice',
                                                    style: new TextStyle(
                                                        fontSize: 16,
                                                        color:
                                                            Color(0xFFDA2079),
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  TextSpan(
                                                    text: ' ৳$discountCharge',
                                                    style: new TextStyle(
                                                        fontSize: 16,
                                                        color:
                                                            Color(0xFFDA2079)),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            child: RichText(
                                              textAlign: TextAlign.start,
                                              text: TextSpan(
                                                children: <TextSpan>[
                                                  TextSpan(
                                                    text: 'বিক্রয়',
                                                    style: new TextStyle(
                                                        fontSize: 10,
                                                        color:
                                                            Color(0xFF646464)),
                                                  ),
                                                  TextSpan(
                                                    text: ' ৳$sellingPrice',
                                                    style: new TextStyle(
                                                        fontSize: 14,
                                                        color:
                                                            Color(0xFF646464),
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  TextSpan(
                                                    text: ' লাভ',
                                                    style: new TextStyle(
                                                        fontSize: 10,
                                                        color:
                                                            Color(0xFF646464)),
                                                  ),
                                                  TextSpan(
                                                    text: ' ৳$profit',
                                                    style: new TextStyle(
                                                        fontSize: 14,
                                                        color:
                                                            Color(0xFF646464),
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            child: ClipRRect(
                                              borderRadius: BorderRadius.circular(25),
                                              child: Container(
                                                color: Color(0xFFFFCCE4),
                                                height: 36,
                                                width: 140,
                                                child: Row(
                                                  children:[
                                                    Expanded(
                                                      child: ElevatedButton(
                                                        style: ElevatedButton.styleFrom(
                                                          shape: CircleBorder(),
                                                          primary: Color(0xFFFFBFDD),
                                                        ),
                                                        onPressed: (){
                                                         /* setState(() {
                                                            if(_count>0){
                                                              _count--;

                                                            }
                                                          });*/
                                                        },
                                                        child: Text('-', style: new TextStyle(color: Colors.white),),
                                                      ),
                                                    ),
                                                    Expanded(child: Text('$_count পিস', style: new TextStyle(fontSize: 14,color: Color(0xFFDA2079),),)),
                                                    Expanded(
                                                      child: ElevatedButton(
                                                        style: ElevatedButton.styleFrom(
                                                          shape: CircleBorder(),
                                                          primary: Color(0xFF6210E1),
                                                        ),
                                                        onPressed: (){
                                                          /*setState(() {
                                                            _count++;
                                                          });*/
                                                        },
                                                        child: Text('+', style: new TextStyle(color: Colors.white),),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                            gridDelegate:
                                const SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 2,
                                    mainAxisExtent: 285,
                                    mainAxisSpacing: 10),
                          ),
                        ),
                      ],
                    ),
                  );
                } else {
                  return Center(
                      child: new Text('No products found!!!',
                          style:
                              new TextStyle(color: Colors.grey, fontSize: 20)));
                }
              } else
                return Center(child: CircularProgressIndicator());
            }
            break;

          default:
            debugPrint("Snapshot " + snapshot.toString());
            return Center(child: new Text('Loading...'));
        }
      },
    );
  }

  Widget _searchServices() {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      elevation: 5,
      child: Form(
        key: _formKey,
        child: Container(
          child: Row(
            children: [
              Expanded(
                child: TextFormField(
                  controller: product,
                  decoration: InputDecoration(
                    contentPadding: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 10.0),
                    enabledBorder: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(15.0)),
                      borderSide: const BorderSide(
                          color: Colors.transparent, width: 0.0),
                    ),
                    border: OutlineInputBorder(),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(15.0)),
                      borderSide:
                          BorderSide(color: Colors.transparent, width: 0.0),
                    ),
                    labelText: 'কাঙ্ক্ষিত পণ্যটি খুঁজুন',
                    labelStyle: TextStyle(color: Colors.grey),
                    suffixIcon: InkWell(
                      onTap: () {
                        if (_formKey.currentState!.validate()) {
                          _searchProducts(product.text);
                        }
                      },
                      child: Icon(
                        Icons.search,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please write the product name';
                    } else {
                      setState(() {
                        product.text = value;
                      });
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _searchProducts(String product) {
    setState(() {
      search = true;
    });
    product_list_future = CallFormsFromAPI(product);
  }
}
