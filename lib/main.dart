// @dart=2.9

import 'package:flutter/material.dart';
import 'package:qtec_product/splash_page.dart';
import 'package:screen_size_test/screen_size_test.dart';


Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MainConfig());
}

class MainConfig extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Qtec',
      showPerformanceOverlay: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: const Color(0xff262545),
        primaryColorDark: const Color(0xff201f39),
        brightness: Brightness.light,
        visualDensity: VisualDensity.adaptivePlatformDensity,

      ),
      /*builder: (context, child) => ScreenSizeTest(
        child: child,
      ),*/
      home: MainConfigExtend (),
    );
  }
}

class MainConfigExtend extends StatefulWidget {

  @override
  _MainPageStateConfig createState() => _MainPageStateConfig();
}

class _MainPageStateConfig extends State<MainConfigExtend> {

  @override
  initState() {
    super.initState();


  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(resizeToAvoidBottomInset: false,


      body: SplashConfig(),//HomePage(),
    );
  }

}
