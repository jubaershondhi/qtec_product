import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/parser.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:badges/badges.dart';

class DetailPageViewExtend extends StatefulWidget {
  String productName;
  String sellingPrice;
  String currentPrice;
  String profit;
  String description;
  List imageList;
  String brandName;

  DetailPageViewExtend({
    required this.productName,
    required this.sellingPrice,
    required this.currentPrice,
    required this.profit,
    required this.description,
    required this.imageList,
    required this.brandName,
  });

  @override
  _DetailPageStateConfig createState() => _DetailPageStateConfig();
}

class _DetailPageStateConfig extends State<DetailPageViewExtend> {

  bool saleCartVisibility = false;
  int _count = 0;

  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: SingleChildScrollView(
          reverse: false,
          child: Container(
              margin: EdgeInsets.all(15),
              child: Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Icon(
                            Icons.arrow_back,
                            color: Colors.black,
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text('প্রোডাক্ট ডিটেইল',
                            style: new TextStyle(fontSize: 20))
                      ],
                    ),
                    SizedBox(height: 10),
                    _searchServices(),
                    SizedBox(height: 10),
                    _imageListWidget(widget.imageList),
                    SizedBox(height: 10),
                    _detailsWidget(
                        widget.productName,
                        widget.sellingPrice,
                        widget.currentPrice,
                        widget.profit,
                        widget.description,
                        widget.brandName),
                  ],
                ),
              )),
        ),
      ),
    );
  }

  Widget _searchServices() {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      elevation: 5,
      child: Container(
        child: Row(
          children: [
            Expanded(
              child: TextFormField(
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  enabledBorder: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(15.0)),
                    borderSide:
                        const BorderSide(color: Colors.transparent, width: 0.0),
                  ),
                  border: OutlineInputBorder(),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(15.0)),
                    borderSide:
                        BorderSide(color: Colors.transparent, width: 0.0),
                  ),
                  labelText: 'কাঙ্ক্ষিত পণ্যটি খুঁজুন',
                  labelStyle: TextStyle(color: Colors.grey),
                  suffixIcon: InkWell(
                    onTap: () {
                      /*if (_formKey.currentState!.validate()) {
                        _searchProducts(product.text);
                      }*/
                    },
                    child: Icon(
                      Icons.search,
                      color: Colors.grey,
                    ),
                  ),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please write the product name';
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _detailsWidget(
      productName, sellingPrice, currentPrice, profit, description, brandName) {
    final String htmlString = description;
    final document = parse(htmlString);
    final String parsedString =
        parse(document.body!.text).documentElement!.text;
    return Container(
      child: Stack(
        alignment: Alignment.center,
        children: [
          Column(
            children: [
              Text(productName,
                  style: new TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF19181B),
                  )),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  RichText(
                    textAlign: TextAlign.start,
                    text: TextSpan(
                      children: <TextSpan>[
                        TextSpan(
                          text: 'ব্রান্ডঃ',
                          style: new TextStyle(
                              fontSize: 14,
                              color:
                              Color(0xFF646464)),
                        ),
                        TextSpan(
                          text: ' $brandName',
                          style: new TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              color:
                              Color(0xFF19181B)),
                        ),
                      ],
                    ),
                  ),
                  Image.asset('assets/ellipses.png'),
                  RichText(
                    textAlign: TextAlign.start,
                    text: TextSpan(
                      children: <TextSpan>[
                        TextSpan(
                          text: 'ডিস্ট্রিবিউটরঃ ',
                          style: new TextStyle(
                              fontSize: 14,
                              color:
                              Color(0xFF646464)),
                        ),
                        TextSpan(
                          text: ' মোঃ মোবারাক হোসেন',
                          style: new TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              color:
                              Color(0xFF19181B)),
                        ),
                      ],
                    ),
                  ),
                ],
              ),

              Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('ক্রয়মূল্যঃ',
                              style: new TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Color(0xFFDA2079),
                              )),
                          Text('৳ ' + currentPrice,
                              style: new TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Color(0xFFDA2079),
                              ))
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('বিক্রয়মূল্যঃ',
                              style: new TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: Color(0xFF000000),
                              )),
                          if(saleCartVisibility==false)...[
                            Container(),
                          ]
                          else...[
                            ClipRRect(
                              borderRadius: BorderRadius.circular(25),
                              child: Container(
                                color: Color(0xFFFFCCE4),
                                height: 36,
                                width: 140,
                                child: Row(
                                  children:[
                                    Expanded(
                                      child: ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                          shape: CircleBorder(),
                                          primary: Color(0xFFFFBFDD),
                                        ),
                                        onPressed: (){
                                          setState(() {
                                            if(_count>0){
                                              _count--;

                                            }
                                          });
                                        },
                                        child: Text('-', style: new TextStyle(color: Colors.white),),
                                      ),
                                    ),
                                    Expanded(child: Text('$_count পিস', style: new TextStyle(fontSize: 14,color: Color(0xFFDA2079),),)),
                                    Expanded(
                                      child: ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                          shape: CircleBorder(),
                                          primary: Color(0xFF6210E1),
                                        ),
                                        onPressed: (){
                                          setState(() {
                                            _count++;
                                          });
                                        },
                                        child: Text('+', style: new TextStyle(color: Colors.white),),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],

                          Text('৳ ' + sellingPrice,
                              style: new TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: Color(0xFF000000),
                              ))
                        ],
                      ),
                      Divider(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('লাভঃ',
                              style: new TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: Color(0xFF000000),
                              )),
                          Text('৳ ' + profit,
                              style: new TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: Color(0xFF000000),
                              ))
                        ],
                      ),
                    ],
                  ),
                ),
              ),

              SizedBox(height: 30,),


              Align(
                  alignment: Alignment.topLeft,
                  child: Text('বিস্তারিত', style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: Color(0xFF323232)),)),
              Text(parsedString, style: new TextStyle(fontSize: 16, color: Color(0xFF646464)),),
            ],
          ),
          if(saleCartVisibility==false)...[
            Positioned(
              bottom: 50,
              child: Container(
                height: 90,
                width: 90,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                        'assets/polygon.png'),
                    fit: BoxFit.fill,
                  ),
                ),
                child: InkWell(
                  onTap: (){
                    setState(() {
                      saleCartVisibility = true;
                    });

                  },
                  child: Column(
                    children:[
                      SizedBox(height:20),
                      Text('এটি', style: new TextStyle(fontSize: 16, color: Colors.white),),
                      Text('কিনুন', style: new TextStyle(fontSize: 16, color: Colors.white),),
                    ],

                  ),
                ),
              ),
            ),
          ]
          else...[

            Positioned(
              bottom: 50,
              child: Badge(
                child: Container(
                  height: 90,
                  width: 90,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                          'assets/polygon.png'),
                      fit: BoxFit.fill,
                    ),
                  ),
                  child: InkWell(
                    onTap: (){
                      setState(() {
                        saleCartVisibility = true;
                      });

                    },
                    child: Column(
                      children:[
                        SizedBox(height:20),
                        Icon(
                          Icons.shopping_cart_outlined,
                          color: Colors.white,
                        ),
                        Text('কার্ট', style: new TextStyle(fontSize: 16, color: Colors.white),),
                      ],

                    ),
                  ),
                ),
                badgeContent: SizedBox(
                    width: 18, height: 18,
                    child:Center(
                      child:Text(_count.toString(), style: TextStyle(
                          color: Color(0xFFDA2079),
                          fontSize: 12
                      )
                      ),
                    )
                ),
                badgeColor: Color(0xFFFFCCE4), //badge background color
              ),

            ),
          ]

        ],
      ),
    );
  }

  Widget _imageListWidget(imageList) {
    return CarouselSlider(
      options: CarouselOptions(
        viewportFraction: 0.5,
        autoPlayAnimationDuration: const Duration(milliseconds: 100),
        autoPlay: true,
        enlargeCenterPage: true,
      ),
      items: widget.imageList
          .map(
            (item) => Center(
              child: Image.network(
                item,
                fit: BoxFit.cover,
              ),
            ),
          )
          .toList(),
    );
  }
}
