
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:qtec_product/product_page.dart';






class SplashConfig extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Qtec',
      showPerformanceOverlay: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: const Color(0xff262545),
        primaryColorDark: const Color(0xff201f39),
        brightness: Brightness.light,
        visualDensity: VisualDensity.adaptivePlatformDensity,

      ),
      // home: LoginPage (title: 'ovivashiBatayon'),
      home: SplashConfigExtend (),
    );
  }
}

class SplashConfigExtend extends StatefulWidget {

  @override
  _SplashPageStateConfig createState() => _SplashPageStateConfig();
}

class _SplashPageStateConfig extends State<SplashConfigExtend> {

  @override
  void initState() {
    super.initState();

    //userLoginStatusCheck();

    Future<void>.delayed(const Duration(milliseconds: 2000), () {
      setState(() {
        Route route = MaterialPageRoute(builder: (context) => LandingPageView());
        Navigator.pushReplacement(context, route);
      });
    });
  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Center(
          // Center is a layout widget. It takes a single child and positions it
          // in the middle of the parent.
          child: Container(
            color: Colors.white,
            child: Column(


              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset('assets/qtec_logo.jpg'),
                Text(
                  'Qtec App', style: new TextStyle(fontSize: 40, color: Colors.deepPurple, fontWeight: FontWeight.bold),
                ),

              ],
            ),
          ),
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

}

